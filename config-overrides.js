const path = require('path');

module.exports = function override(config) {
  config.resolve = {
    ...config.resolve,
    alias: {
      ...config.alias,
      domain: path.resolve(__dirname, 'src/domain'),
      infrastructure: path.resolve(__dirname, 'src/infrastructure'),
      presentation: path.resolve(__dirname, 'src/presentation'),
      test: path.resolve(__dirname, 'src/test'),
    },
  };
  return config;
};
