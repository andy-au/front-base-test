import { Box, MenuIcon } from '@chakra-ui/react';

type Props = {
  toggle: () => {};
  isOpen: boolean;
};
const MenuToggle = ({ toggle, isOpen }: Props) => {
  return (
    <Box display={{ base: 'block', md: 'none' }} onClick={toggle}>
      {isOpen ? <MenuIcon /> : <MenuIcon />}
    </Box>
  );
};

export default MenuToggle;
