import { Link, Button } from '@chakra-ui/react';
import { CSSProperties, ReactElement } from 'react';
import styled from 'styled-components';

interface ButtonLinkProps {
  title: string;
  href: string;
  color: string;
  id: string;
  isExternal: boolean;
  style?: CSSProperties;
  variant: 'solid' | 'outline';
  leftIcon?: ReactElement;
  rightIcon?: ReactElement;
}

const ButtonLink = ({
  href,
  title,
  color,
  id,
  isExternal,
  style,
  variant,
  leftIcon,
  rightIcon
}: ButtonLinkProps) => {
  return (
    <LinkContainer
      _hover="none"
      style={style}
      href={href}
      data-testid={id}
      isExternal={isExternal}
    >
      <Button
        style={{ width: '100%' }}
        transition="all 0.2s cubic-bezier(.08,.52,.52,1)"
        colorScheme={color}
        variant={variant}
        leftIcon={leftIcon}
        rightIcon={rightIcon}
      >
        {title}
      </Button>
    </LinkContainer>
  );
};

const LinkContainer = styled(Link)`
  text-decoration: none;
  &:focus,
  &:hover,
  &:visited,
  &:link,
  &:active {
    text-decoration: none;
  }
`;

export default ButtonLink;
