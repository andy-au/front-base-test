import React from 'react';
import { Box, Image } from '@chakra-ui/react';
// import styled from 'styled-components';

const mainLogo = require('../../assets/light-logo.png');

const Logo = () => {
  return (
    <Box width={{ base: '150px', sm: '220px' }} margin="0 auto">
      <Image src={mainLogo} alt="Main Logo" />
    </Box>
  );
};

export default Logo;

// const ImageLogo = styled(Image)`
//   display: block;
//   margin-left: auto;
//   margin-right: auto;
//   width: 250px;

//   @media (max-width: 768px) {
//     width: 200px;
//   }
// `;
