/* eslint-disable no-useless-escape */
import { useForm } from 'react-hook-form';
import styled from 'styled-components';
import {
  FormErrorMessage,
  FormLabel,
  FormControl,
  Input,
  Button,
  Link,
  Text
} from '@chakra-ui/react';
import { login } from '../../../infrastructure/services/auth/auth';

const Login = () => {
  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting }
  } = useForm();

  const onSubmit = async (values: any) => {
    console.log(values);
    const asd = await login(values.email, values.password);
    console.log(asd);
    return new Promise((resolve: any) => {
      setTimeout(() => {
        alert(JSON.stringify(values, null, 2));
        resolve();
      }, 3000);
    });
  };

  return (
    <Container>
      <form onSubmit={handleSubmit(onSubmit)}>
        <FormControlContainer variant="floating" isInvalid={errors.email}>
          <Input
            id="email"
            autoComplete="off"
            placeholder=" "
            {...register('email', {
              required: 'Campo requerido',
              pattern: {
                value:
                  /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                message: 'Email invalido'
              }
            })}
          />
          <FormLabel htmlFor="email" size="lg">
            Ingrese su Email
          </FormLabel>
          <FormErrorMessage>
            {errors.email && errors.email.message}
          </FormErrorMessage>
        </FormControlContainer>

        <FormControlContainer
          variant="floating"
          id="password"
          isInvalid={errors.password}
        >
          <Input
            type="password"
            maxLength={32}
            placeholder=" "
            {...register('password', {
              required: 'Campo requerido'
            })}
          />
          <FormLabel htmlFor="password">Ingrese su Contraseña</FormLabel>
          <FormErrorMessage>
            {errors.password && errors.password.message}
          </FormErrorMessage>
        </FormControlContainer>

        <ContainerButtons>
          <Button
            isLoading={isSubmitting}
            width="45%"
            loadingText="Validando"
            colorScheme="pink"
            variant="solid"
            spinnerPlacement="start"
            type="submit"
          >
            Login
          </Button>
          <Text fontSize="xs" marginTop="0.5rem">
            Si no tienes cuenta registrate{' '}
            <Link href="/" color="teal.500">
              <b>Aquí</b>
            </Link>
          </Text>
        </ContainerButtons>
      </form>
    </Container>
  );
};

const Container = styled.div`
  padding: 3rem 0;
  max-width: 450px;
  height: 50px;
  display: block;
  margin-left: auto;
  margin-right: auto;
`;

const FormControlContainer = styled(FormControl)`
  margin-bottom: 2rem;
`;
const ContainerButtons = styled.div`
  margin-top: 2.5rem;
  text-align: -webkit-center;
`;

export default Login;
