import { Calendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';
import { Box } from '@chakra-ui/react';
import messages from './calendar-message-es';

import 'react-big-calendar/lib/css/react-big-calendar.css';
import './style.css';

import 'moment/locale/es';

moment.locale('es');

const localizer = momentLocalizer(moment);

const events = [
  {
    title: 'Cumpleaños',
    start: moment().toDate(),
    end: moment().add(2, 'hour').toDate(),
    bgColor: '#fafafa'
  }
];
// type PropEvent = {
//   event: string;
//   start: stringOrDate;
//   end: stringOrDate;
//   isSelected: boolean;
// };

const CalendarScreen = () => {
  const eventStyleGetter = (
    event: any,
    start: any,
    end: any,
    isSelected: any
  ) => {
    console.log(event, start, end, isSelected);
    const style = {
      backgroundColor: '#3ea6ad',
      borderRadius: '0px',
      opacity: 0.8,
      display: 'block',
      color: 'white'
    };
    return { style };
  };
  return (
    <Box>
      <Calendar
        className="calendar-screen"
        localizer={localizer}
        events={events}
        startAccessor="start"
        endAccessor="end"
        messages={messages}
        eventPropGetter={eventStyleGetter}
      />
    </Box>
  );
};

export default CalendarScreen;
