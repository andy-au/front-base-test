import { useMediaQuery } from '@chakra-ui/react';
import styled from 'styled-components';
import Logo from '../../atoms/Logo/Logo';
import Login from '../../molecules/login/Login';

const LoginPage = () => {
  const [isLowerThan400] = useMediaQuery('(max-width: 400px)');

  return (
    <Container isLowerThan400={isLowerThan400}>
      <ContainerLogin>
        <Logo />
        <Login />
      </ContainerLogin>
    </Container>
  );
};

const Container = styled.div<{ isLowerThan400: boolean }>`
  padding: ${(props) => (props.isLowerThan400 ? '20px 10px' : '50px 10px')};
  height: 100vh;
  width: 100vw;
  display: flex;
  justify-content: center;
  background-image: linear-gradient(
    to right top,
    #d16ba5,
    #c777b9,
    #ba83ca,
    #aa8fd8,
    #9a9ae1,
    #8aa7ec,
    #79b3f4,
    #69bff8,
    #52cffe,
    #41dfff,
    #46eefa,
    #5ffbf1
  );
`;

const ContainerLogin = styled.div`
  box-shadow: 1px 3px 20px 8px rgb(0 0 0 / 9%);
  background-color: #fff;
  padding: 50px 10px;
  max-height: 600px;
  width: 500px;
  border-radius: 25px;
  -moz-border-radius: 25px;
`;

export default LoginPage;
