import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

const API_URL = 'http://localhost:3100/api/v1/user/signin';

// export const register = (username, email, password) => {
//   return axios.post(API_URL + 'signup', {
//     username,
//     email,
//     password,
//   });
// };

const login = async (email: string, password: string) => {
  const body = {
    email,
    password
  };

  const requestOptions: AxiosRequestConfig = {
    method: 'POST',
    url: API_URL,
    data: body,
    headers: {},
    timeout: 5000
  };

  try {
    const response: AxiosResponse = await axios(requestOptions);
    if (response.data.accessToken) {
      localStorage.setItem('token', response.data.accessToken);
      localStorage.setItem('refresh_token', response.data.refreshToken);
    }
    return response.data;
  } catch (error) {
    console.log(Error);
  }
};

const logout = () => {
  localStorage.removeItem('user');
};

// const authHeader = () => {
//   const user = JSON.parse(localStorage.getItem('user') as string);
//   if (user && user.accessToken) {
//     return { Authorization: user.accessToken };
//   } else {
//     return {};
//   }
// };

export { login, logout };
