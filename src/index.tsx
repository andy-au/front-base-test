import { ChakraProvider, extendTheme } from '@chakra-ui/react';
import { ReactQueryDevtools } from 'react-query/devtools';
import { QueryClient, QueryClientProvider } from 'react-query';
import ReactDOM from 'react-dom';

import App from './App';

import Navbar from './components/molecules/navbar/Navbar';

// import { CheckAuthentication } from './utils/CheckAuthentication';s

const colors = {
  brand: {
    900: '#1a365d',
    800: '#153e75',
    700: '#2a69ac'
  }
};

// const theme = extendTheme({ colors });
const activeLabelStyles = {
  transform: 'scale(0.9) translateY(-32px) translateX(-16px)'
};

const theme = extendTheme({
  initialColorMode: 'dark',
  useSystemColorMode: true,
  ...colors,
  components: {
    Form: {
      variants: {
        floating: {
          container: {
            _focusWithin: {
              label: {
                ...activeLabelStyles
              }
            },
            'input:not(:placeholder-shown) + label, .chakra-select__wrapper + label':
              {
                ...activeLabelStyles
              },
            label: {
              top: 0,
              left: 0,
              zIndex: 2,
              position: 'absolute',
              backgroundColor: 'white',
              pointerEvents: 'none',
              mx: 3,
              px: 1,
              my: 2
            }
          }
        }
      }
    }
  }
});
const queryClient = new QueryClient();

// store={store}
ReactDOM.render(
  <QueryClientProvider client={queryClient}>
    <ChakraProvider theme={theme}>
      <Navbar />
      <App />
      <ReactQueryDevtools initialIsOpen={false} />
    </ChakraProvider>
  </QueryClientProvider>,

  document.getElementById('root')
);
